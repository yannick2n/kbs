<?php
$db ="mysql:host=127.0.0.1;dbname=wideworldimporters;port=3306";
$user = "root";
$pass = "";
$pdo = new PDO($db, $user, $pass);
try {
    $product_array = $pdo->prepare("SELECT * FROM stockitems");
    $product_array->execute();
}
catch (PDOException $e) {
    print($e->getMessage());
}
// loop langs alle rijen
while ($row = $product_array->fetch()) {



    ?>
    <div class="row">
        <div class="product-item">
            <form method="post" action="index.php?action=add&code=<?php echo $row["stockitemid"]; ?>">
                <div class="product-image"><img src="<?php echo $row["photo"]; ?>"></div>
                <div class="product-tile-footer">
                    <div class="product-title"><?php echo $row["stockitemname"]; ?></div>
                    <div class="product-price"><?php echo "€".$row["recommendedretailprice"]; ?></div>
                    <div class="cart-action"><input type="text" class="product-quantity" name="quantity" value="1" size="2" /><input type="submit" value="Add to Cart" class="btnAddAction" /></div>
                </div>
            </form>
        </div>
    </div>
    <?php
}
?>