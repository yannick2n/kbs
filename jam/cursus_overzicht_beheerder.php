<?php include "menu.php"; ?>

<article multilinks-noscroll="true" id="content" class="clearfix">
<?php
        include "connect-site.php";
		
$actie = "";
$ids = "";
$geselecteerdCurus = array();

        //de ingelogde sessie bekijken
        if (isset($_SESSION['inlognaam'])) {
            $email1 = $_SESSION['inlognaam'];
            $preKlantID = "SELECT ingelogdStatus FROM klant WHERE email = :inlognaam";
            $klantID = $dbh->prepare($preKlantID);
            $klantID->bindValue(":inlognaam", $email1, PDO::PARAM_STR);
            $klantID->execute();
            foreach ($klantID as $klant) {
                if ($klant['ingelogdStatus'] == 3) {
				
if (isset($_POST['ids'])){
if (isset($_POST['actie'])) {
    $actie = $_POST['actie'];

//Verwijderen van geselecteerde curusu//

        if ($actie == "verwijderen") {

            $ids = $_POST['ids'][0];
            $st = "DELETE FROM cursus WHERE cursusID =:ids ";

            $sth = $dbh->prepare($st);
            $sth->bindValue(':ids', $ids, PDO::PARAM_STR);


            $sth->execute();
        }
}

//Wijzigen van geselecteerde cusus//	
		if ($actie == "wijzigen") {
       		$nr = $_POST['ids'][0];
        {
//Ophalen van bestaande gegevens en plaatsen in wijzigvelden//							
            $sql3 = $dbh->prepare("
							SELECT soort, opmerking, prijs, duurVanCursus, maxAantalMensen, datum, startTijd
							FROM cursus WHERE cursusID=$nr");
            $sql3->execute();
            $result2 = $sql3->fetchAll(PDO::FETCH_BOTH);
            foreach ($result2 as $geselecteerdCursus);
        }
    }
}

//Wijzigen gewijzigde gegevens  waneer op wijzig wordt geklikt //
            if ($actie == "wijzig") {
                $cursusID = $_POST['cursusID'];
				$soort = $_POST['soort'];
				$opmerking = $_POST['opmerking'];
                $prijs = $_POST['prijs'];
				$duurVanCursus = $_POST['duurVanCursus'];
				$maxAantalMensen = $_POST['maxAantalMensen'];
				$datum = $_POST['datum'];
				$startTijd = $_POST['startTijd'];

				$std = "UPDATE cursus SET soort='$soort',opmerking='$opmerking',prijs=$prijs,duurVanCursus=$duurVanCursus,maxAantalMensen='$maxAantalMensen',datum=$datum,startTijd='$startTijd' WHERE cursusID=$cursusID";

                $stdd = $dbh->prepare($std);
                $stdd->execute();
            }

// Alles van cursus ophalen uit database
        $sth = $dbh->prepare("SELECT * FROM cursus");
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_BOTH);
        ?>
        
  <h1>Cursus</h1>

<!-- Thead van tabel met gegevens over de verschillende cursussen-->
  <form action="./cursus_overzicht_beheerder.php" method="POST">
    <table class="table table-bordered table-striped table-hover">
      <thead>
        <tr>
          <td>Selectie</td>
          <td>Soort cursus</td>
          <td>Beschrijving</td>
          <td>Prijs (&euro;)</td>
          <td>Duur (min)</td>
          <td>Max aantal mensen</td>
          <td>Datum</td>
          <td>Startijd</td>
        </tr>
      </thead>

<!-- Gegevens uit database in tabel printen -->
	  <?php foreach ($result as $cursus) {  ?>
		
        <tr>
<?php     print"<td> <input style= inline type=\"radio\" name=\"ids[]\" value=\"$cursus[cursusID]\"></td>"  ?>
		        <td><a href="cursus_details.php?cursusID=<?php print ($cursus['cursusID'])?>"><?php print($cursus['soort']); ?></a></td>
        <?php
print("
    			<td>" . ($cursus['opmerking']) . "</td>
			    <td>" . ($cursus['prijs']) . "</td>
			    <td>" . ($cursus['duurVanCursus']) . "</td>
    			<td>" . ($cursus['maxAantalMensen']) . "</td>
				<td>" . ($cursus['datum']). "</td>
				<td>" . ($cursus['startTijd']). "</td>
		</tr>
");

}

?>
 
    </table>
    <br />
          <table>
            <?php
//Formulier met de bestaande gegevens en een wijzig knop tonen.//			 					
            if ($actie == "wijzigen") {
                    print"<td><input type=\"hidden\" name=\"cursusID\" value=\"$nr\"/>";
                    print"<td>Naam: <input type=\"text\" value=\"" . $geselecteerdCursus['soort']."\" name=\"soort\"/><br/>";
					print"<td>Omschrijving: <input type=\"text\" value=\"" . $geselecteerdCursus['opmerking'] . "\" name=\"opmerking\"/><br/>";
					print"<td>Prijs (&euro;): <input type=\"text\" value=\"".$geselecteerdCursus['prijs']."\" name=\"prijs\"/><br/>";
                    print"<td>Duur: <input type=\"text\" value=\"".$geselecteerdCursus['duurVanCursus']."\" name=\"duurVanCursus\"/><br/>";
                    print"<td>maxAantalMensen: <input type=\"text\" value=\"".$geselecteerdCursus['maxAantalMensen']."\" name=\"maxAantalMensen\"/><br/>";
                    print"<td>Datum: <input type=\"date\" value=\"".$geselecteerdCursus['datum']."\" name=\"datum\" placeholder=\"dd-mm-jj\"/><br/>";
                    print"<td>StartTijd: <input type=\"time\" value=\"".$geselecteerdCursus['startTijd']."\" name=\"startTijd\"/><br/>";
					print"<td><input type='submit' value='wijzig' name='actie'>";
			}//elseif($actie == "wijzig"){
				
			//}
print"</tr>";
             
            ?>
        </table>  


<!-- Wijzig button en Verwijder button en Toevoegen button -->
<div  style="text-align:center;">
<input class="wijzigen" type="submit" value="wijzigen" name="actie" />
<input class="verwijderen" type="submit" value="verwijderen" name="actie" />
</div>
</form>
  <form action="./cursus_plaatsen.php" method="POST">
        <div  style="text-align:center;">
     <input class="toevoegen" type="submit" value="toevoegen" />
        </div>
</form>
  
  <?php }
			}
		}else{
				print "U heeft niet de goede rechten.";
			}
		?>

</article>

<body>

</body>
</html>