$(function(){
	
	$(window).scroll(function(){
		if(window.pageYOffset > 600){
			$('#to-top').addClass('show');	
		} else {
			$('#to-top').removeClass('show');	
		}
	});
	if ($(window).width() > 500){
		$(".fb").fancybox({
			openEffect	: 'fade',
			closeEffect	: 'none',
			nextEffect	: 'fade',
			prevEffect	: 'fade',
			padding		: [0,250,0,0],
			margin		: 50,
			maxWidth	: 800,
			maxheight	: 500,
			helpers : {
				title : {
					type : 'inside'
				}
			},
			tpl			: {
				wrap     : '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div><p class="call-to-action"><a href="/sign-up/" class="button">'+$('#blade-matrix').data('buynow')+'</a></p></div></div>'
			}
		});
	} else {
		$(".fb").click(function(e){
			e.preventDefault();
		})	
	}
	$(".lbframe").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	
	$(".lbframe-small").fancybox({
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	
	$('a.scroll').click(function(e){
		e.preventDefault();
		var elementId = $(this).attr('href');
		
		$('html, body').animate({
         scrollTop: $(elementId).offset().top-40
     	}, 500, 'swing');
	});
	
	//SVG SUPPORT DETECTION
	if (!Modernizr.svg){
		replace_svg();
		$('body').addClass('.no-svg');
	}
	

	//BEVESTIG KNOP
	$('.bevestigen .button').click(function(e){
		if ($(this).attr('class').indexOf('inactive') >= 0){
			e.preventDefault();
			parent = $(this).parents('.bevestigen');
			$('.bevestig-box', parent).animate({'backgroundColor':'#46adb2'}, 100);
			$('.bevestig-box', parent).delay(100).animate({'backgroundColor':'#ffffff'}, 1000);
		}
	});

	//OPEN POPUP
	$('a.open-popup').click(function(e){
		e.preventDefault();
		$('.popup').addClass('hidden');
		var id = $(this).attr('href');
		$(id).toggleClass('hidden');
		$('.overlay-close').removeClass('hidden');
		$('html, body').animate({
         scrollTop: $(id).offset().top-50
     	}, 500, 'swing');
	});
	
	//CLOSE POPUP
	$('.close-popup').click(function(e){
		e.preventDefault();
		var parent = $(this).parent();
		$('.popup').addClass('hidden');
	});


	//WAYPOINTS MAIN NAV
	if ($('body').attr('class').indexOf('home') !== -1 && $('nav#main ul:first').attr('id').indexOf('main-nav') !== -1 ){
		$('#content article').waypoint(function(event, direction){
			$('#main-nav li').removeClass('active');
			var the_id = $(this).attr('id');
			var parent = $('#main-nav a[href="#'+the_id+'"]').parent();
			$(parent).addClass('active');
		}, { 
			offset:80
		});
		
		// ANIMATE PRICE STRUCTURE
		if ($(window).width() > 1000){
			
			init_price_structure();
		
			$('#how-we-do-it-animation').waypoint(function(event, direction){
				price_structure();
			}, {
				offset:400, 
				triggerOnce: true
			});
		}
	}
	init_animate_shield_swords();
		$('.shield-swords').waypoint(function(event,direction){
			animate_shield_swords();
		}, {
			offset:400, 
			triggerOnce: true
		});
	
	if ($(window).width() > 400){
		slideShow();
	}
	
	$('input, textarea').placeholder();
	



});






function slideShow(){
	slide_width();
	
	//DISABLE AND RESET SLIDESHOW FOR MOBILE
	$(window).resize(function(event){
		slide_width();
	});
	$('.slides-container').attr('data-current-slide', 0);
	$('.slides-viewport').each(function(){
		var slide_count = $('.slides-container .slide', this).length;	
		
		//Slide controls
		init_slide_controls($(this), slide_count);
		
			//NEXT BACK
			$('.next-slide', this).click(function(e){
				e.preventDefault();
				the_slide($(this).parent(), 'next', '');
			});
			
			$('.previous-slide', this).click(function(e){
				e.preventDefault();
				the_slide($(this).parent(), 'previous', '');
			});
			
			
			//BULLETS
			$('.bullet-controls li', this).click(function(e){
				var slides_container = $(this).parent().parent();
				the_slide(slides_container, 'goto', parseInt($(this).attr('data-slide')));
			});
	});
	
	//LINKS
	$('.slide-link').click(function(e){
		e.preventDefault();
		
		if ($(window).width() > 740){
			var id = $(this).attr('data-viewport-id');
			var target = $('#'+id+' .slides-container');
			the_slide($(target), 'goto', $(this).attr('data-goto-slide'));
		}
	})
	
	//--------------------------------------------------------------------
	// NEXT SLIDE ACTION
	//--------------------------------------------------------------------
	function the_slide(target, type, destination){
		var current_slide = parseInt($(target).attr('data-current-slide'));
		var slide_count = $('.slide',target).length;
		var target_parent = $(target).parent();
		
		//Enable/Disable Previous/Next
		$('.previous-slide', target_parent).removeClass("hidden");
		$('.next-slide', target_parent).removeClass("hidden");
		
		switch(type){
			case 'next':
				if (current_slide < slide_count-1){
					next_slide = current_slide + 1;
				} else {
					next_slide = 0;
				}
			break;
			
			case 'previous':
				if (current_slide > 0){
					next_slide = current_slide - 1;
				} else {
					next_slide = slide_count-1;	
				}
			break;
			
			case 'goto':
				next_slide = destination;
				
			break;
		}
		
		// Use animate when someone uses IE9 or lower
		if ($('html').attr('class').indexOf('lt-ie10') >= 0){
			$(target).animate({'marginLeft':(-100*next_slide)+'%'});	
		} else {
			$(target).css('marginLeft', (-100*next_slide)+'%');
		}
		
		$(target).attr('data-current-slide', next_slide);
		
		//Activate bullets
		$('.bullet-controls li',target_parent).removeClass('active');
		$('.bullet-controls li[data-slide="'+next_slide+'"]', target_parent).addClass('active');
		
		//DISABLE PREVIOUS NEXT
		if (next_slide == 0){
			$('.previous-slide', target_parent).addClass('hidden');
		} else if (next_slide == (slide_count-1)){
			$('.next-slide', target_parent).addClass('hidden');
		}
	}
	
	//--------------------------------------------------------------------
	// INITIATE SLIDE CONTROLS	(all, nextback, bullets, none)
	//--------------------------------------------------------------------
	function init_slide_controls(target, slide_count){

		var slide_control_type = $(target).attr('data-slide-controls');
		
		switch (slide_control_type ){
			
			case 'all':
				nextback_controls(target);
				bullet_controls(target, slide_count);
			break;
				
			case 'nextback':
				nextback_controls(target);
			break;
			
			case 'bullets':
				bullet_controls(target, slide_count);
			break;
			
			case 'auto':
				this.target = target;
				this.t = setInterval(function(){
					the_slide($('.slides-container', this.target), 'next', '')
				}, 4000);
			break;
		
		}
	}
	
	//--------------------------------------------------------------------
	// NEXT/BACK CONTROLS
	//--------------------------------------------------------------------	
	function nextback_controls(target){
		$('.slides-container', target).prepend('<a class="previous-slide slide-control hidden" data-type="prev" href="#"></a>');
		$('.slides-container', target).append('<a class="next-slide slide-control" data-type="next" href="#"></a>');
	}
	
	//--------------------------------------------------------------------
	// NEXT/BACK CONTROLS
	//--------------------------------------------------------------------
	function bullet_controls(target, slide_count){
		var bullet_controls_html = '<ul class="bullet-controls dark slide-control">';
		$('.slide', target).each(function(index){
			bullet_controls_html += '<li';
			
			//FIRST BULLET
			if (index==0){
				bullet_controls_html += ' class="active first"';
			}
			bullet_controls_html +=  ' data-slide="'+index+'">';
			
			//ADD LABEL IF EXISTS
			if (label = $(this).attr('data-name')){
				bullet_controls_html +=  '<span>'+label+'</span>';
			}
			bullet_controls_html += '</li>';
		});
		bullet_controls_html += '</ul>';
		$('.slides-container', target).append(bullet_controls_html);	
	}
	//--------------------------------------------------------------------
	// RESET SLIDES
	//--------------------------------------------------------------------
	function reset_slides(){
		$('.slides-container').css({'marginLeft':'0'});
		the_slide($('.slides-container'), 'goto', 0);
	}
	//--------------------------------------------------------------------
	// SLIDE WIDTH (for IE7 and lower)
	//--------------------------------------------------------------------
	function slide_width(){
		if ($('html').attr('class').indexOf('lt-ie8') >= 0){
			$('.slides-container').each(function(){
				var slide_count = $('.slide', this).length;
				var parent = $(this).parent();
				var width = parent.width();
				$('.slides-container', this).css('width', (width*slide_count)+'px');
				$('.slide', this).css('width', (width-40)+'px');
				$('.slide', this).css('paddingLeft', '20px');
				$('.slide', this).css('paddingRight', '20px');
			});
		}	
	}
	
}

//--------------------------------------------------------------------
// Price structure
//--------------------------------------------------------------------
function init_price_structure(){
	$('.price-structure li.step').css({'opacity':0, 'marginLeft':'0px'});
	$('.price-structure.one li.price .inner-price').html('0');
	$('.price-structure.two li.price .inner-price').html('0');	
}
function price_structure(){
	
	$('.price-structure').each(function(index){
		var total_childs = $('li', this).length;
		this.price = 0;
	});
	animate_price_structure($('.price-structure li.first').first());
	document.current_price_counter = 0;
	$('.price-structure li.price').removeClass('finished');
	$('.price-structure li.price').attr('data-status','animate');
	document.t = setInterval(function(){
		if (document.current_price_counter == 0){
			animate_price($('.price-structure li.price').first());
		} else {
			animate_price($('.price-structure.two li.price'));
		}
	}, 190);
}
function animate_price_structure(target){
	var next = $(target).next();
	if($(next).attr('class').indexOf('price') !== 0){
		$(next).animate({
			'opacity':1,
			'marginLeft':'28px'
		}, 700, function(){
			animate_price_structure(this);
		});
	} else {
		next = $(next).parent();
		next = $(next).next();
		next = $('li.first', next).next();
		$(next).delay(700).animate({
			'opacity':1,
			'marginLeft':'28px'
		}, 700, function(){
			animate_price_structure(this);
		});	
	}
}
function animate_price(target){
	var max_price = parseInt($(target).attr('data-price'));
	var step = parseInt($(target).attr('data-steps'));
	//alert(step);
	if (document.current_price_counter == 0){
		var current_price = parseInt($('span .inner-price', target).html());
	} else {
		var current_price = parseInt($('span a .inner-price', target).html());
	}
	var new_price = current_price + step;
	if(current_price < max_price){
		if (document.current_price_counter == 0){
			$('span .inner-price', target).html(new_price);
		} else {
			$('span .inner-price', target).html(new_price);
		}
	} else {
		if (document.current_price_counter > 0){
			$(target).addClass('finished');
			$(target).attr('data-status', 'finished');
		}
		document.current_price_counter++;
	}
	if (document.current_price_counter > 1){
		clearInterval(document.t);
	}
}

//--------------------------------------------------------------------
// HOME ANIMATIONS
//--------------------------------------------------------------------
function init_animate_shield_swords(){
	$('.shield-swords .the-sword-left').css({'marginLeft':'-122px','top':'39px'});
	$('.shield-swords .the-sword-right').css({'marginLeft':'20px','top':'39px'});
}
function animate_shield_swords(){
	$('.shield-swords .the-sword-left').delay(100).animate({'marginLeft':'-48px','top':'113px'}, 500, function(){
		$('.shield-swords .the-sword-left').animate({'marginLeft':'-51px','top':'110px'}, 200);	
	});
	$('.shield-swords .the-sword-right').delay(100).animate({'marginLeft':'-54px','top':'113px'}, 500, function(){
		$('.shield-swords .the-sword-right').animate({'marginLeft':'-51px','top':'110px'}, 200);	
	});	
}
