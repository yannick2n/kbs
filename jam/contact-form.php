<?php
include 'menu.php'; 
?>
<article multilinks-noscroll="true" id="content" class="clearfix">
				<h1>Contact</h1> 

<img src="79043071_5_Qxjj.jpeg" width="190" height="237">

<div multilinks-noscroll="true" class="primary-layer slide" data-name="Contact"> <div class="summary">
      <h2>Over ons</h2>

      <p> Sinds ik in 2008 verhuisd ben naar Zwolle ben ik door een goede vriendin gemotiveerd om jam te gaan maken. Zodoende ben ik aan het experimenteren geslagen met verschillende combinaties vruchten en specerijen. Door de jam met alleen de meest verse producten (die per seizoen verschillen) te fabriceren ontstaat daaruit de meest perfecte biologische jam. 
Laat u verassen door alle verschillende combinaties die beschikbaar zijn. Probeert u bijvoorbeeld de overheerlijke ‘appeltaartjam’.  Zelf een combinatie jam in gedachte, maar staat hij er niet tussen? 
Laat het me weten via onderstaand contact formulier en wie weet is er een mogelijkheid tot aanbod.
Workshop bijwonen? Tijdens workshops kunt u ervaring opdoen met het maken van onder andere jam en leert u nieuwe combinaties jam  maken die u anders nooit had geprobeerd. Klik op het kopje cursus om u in te schrijven (registratie verplicht).

	  </p> <br> 
   	 <p>Bij vragen e.d. kunt u contact opnemen via onderstaande gegevens of formulier.</p>
						</div>
</article>
<div class="container">

			
					<div class="one-half first">
						<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.nl/maps?f=q&amp;source=s_q&amp;hl=nl&amp;geocode=&amp;q=alm+23&amp;aq=&amp;sll=52.526693,6.10695&amp;sspn=0.006345,0.01545&amp;ie=UTF8&amp;hq=&amp;hnear=Alm+23,+8032+BK+Zwolle,+Overijssel&amp;t=m&amp;z=14&amp;iwloc=A&amp;output=embed">
                        </iframe> 
						<h2 >Jamfactory</h2>
						<p class="foot-note">Alm 23<br>
						8032BK Zwolle</p>
						<p class="foot-note">038-4544759</p>
					</div>

<?php  
        // Bevestig dat formulier is verzonden 
        if (isset($_GET['s'])) echo "<div class=\"alert alert-success\">".$_GET['s']."</div>";  

        // Controleer of formulier fouten bevat
        elseif (isset($_GET['e'])) echo "<div class=\"alert alert-error\">".$_GET['e']."</div>";  
?>  

        <form method="POST" action="contact-form-submission.php" class="one-half first">
			 <div class="control-group">
                <label class="control-label" form="input1">Naam:</label>
                <div class="controls">
                    <input type="text" name="contact_name" id="input1" placeholder="Je naam">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" form="input2">E-mailadres:</label>
                <div class="controls">
                    <input type="text" name="contact_email" id="input2" placeholder="Je e-mailadres">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" form="input3">Bericht:</label>
                <div class="controls">
                    <textarea name="contact_message" id="input3" rows="8" class="span5" placeholder="Het bericht dat je wilt versturen."></textarea>
                </div>
            </div>
            <div class="form-actions"> 
                <input type="hidden" name="save" value="contact">
                <button type="submit" class="btn btn-primary"> Verstuur</button>
            </div>
        </form>
    </div>
</body>
</html>

