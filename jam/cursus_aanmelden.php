<!DOCTYPE html>

<?php
include "menu.php";
SESSION_START();
?>
<article multilinks-noscroll="true" id="content" class="clearfix">
<html>
    <head>
        <link href="../stylesheet/style.css" rel="stylesheet" type="text/css" />
        <link href="../stylesheet/css/bootstrap.min.css" rel="stylesheet" />

        <?php
       include "connect-site.php";
//cursus klaarzetten
        $datee = date('Y-m-d');
        $cursus = $dbh->prepare("SELECT C.cursusID, soort, C.datum, opmerking, duurVanCursus, prijs, startTijd
FROM cursus C
LEFT JOIN inschrijving I ON C.cursusID = I.cursusID
WHERE C.datum > :datee
GROUP BY cursusID
HAVING 15 > 'aantal_personen'");
        $cursus->bindValue(":datee", $datee, PDO::PARAM_STR);
        $cursus->execute();
        $result = $cursus->fetchAll(PDO::FETCH_BOTH);
        if (isset($_SESSION['status'])){
        if ($_SESSION['status'] > 1) {
            ?>
        </head>
        <body>
            <h1 align="center">aanmelden voor cursus</h1>
            <h4 align="center">U kunt maximaal 3 mensen meenemen</h4>

            <form action="./cursus_aangemeld.php" method="POST">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <td> Naam</td>
                            <td> Datum</td>
                            <td>waar gaat het over?</td>
                            <td>lengte</td>
                            <td> Starttijd</td>
                            <td>prijs</td>
                            <td>aanmelden</td>
                        </tr>
                    </thead>

                    <?php
// De gegevens ophalen
                    //het tabel weergeven
                    foreach ($result as $result1) {
                        $newdate = date('d-m-Y', strtotime($result1['datum']));
                        print "<tr>";
                        print"<td> " . ($result1['soort']) . " </td>";
                        print"<td> " . ($newdate) . "</td>";
                        print"<td> " . ($result1['opmerking']) . " </td>";
                        print"<td>" . ($result1['duurVanCursus']) . " </td>";
                        print"<td>" . ($result1['startTijd']) . " </td>";
                        print"<td>&euro; " . ($result1['prijs']) . "p.p.</td> ";
                        print "<td><input type=\"radio\" name=\"id\" value=\"$result1[cursusID]\"</td>";
                    }

                    print"<tr><td><input style= inline type=\"submit\" name=\"ids\" value=\"aanmelden\"></td>";
                    print"<td>aantal mensen:<select name=\"nummer\">";
                    print"<option value=\"1\">1</option>";
                    print"<option value=\"2\">2</option>";
                    print"<option value=\"3\">3</option>";
                    print"</select></td>";
                    print "</tr>";
                    ?>
                </table>
            </form>
            <?php
        } else {
            print "<body>";
            print "Om je voor een cursus in te schrijven moet je geregistreerd zijn, maak een account of log in!";
            print "</body>";
        }
        }else {
                        print "<body>";
            print "Om je voor een cursus in te schrijven moet je geregistreerd zijn, maak een account of log in!";
            print "</body>";
        }
        ?>