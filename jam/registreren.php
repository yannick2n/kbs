<?php
include 'menu.php';
include 'functions.php';
include 'connect-site.php';
?>
	
<bodyfoutmelding>	
        <div multilinks-noscroll="true" id="content" role="main" class="clearfix"/>

<registratie>
				 <h1> Registratie </h1>

            <form class="registratie" action="registreren.php" method="POST">
                <table>
            <tr>
                <td>Voornaam:</td> <td><input type="text" name="voornaam" value="<?php if(isset($_POST['registreer'])){echo $_POST['voornaam'];}?>" placeholder="Henk"/></td>
            </tr>
            <tr>
                <td>Tussenvoegsel:</td> <td><input type="text" name="tussenvoegsel" value="<?php if(isset($_POST['registreer'])){echo $_POST['tussenvoegsel'];}?>" placeholder="de"/></td>
            </tr>
            <tr>
                <td>Achternaam:</td> <td><input type="text" name="achternaam" value="<?php if(isset($_POST['registreer'])){echo $_POST['achternaam'];}?>" placeholder="Vries"/></td>
            </tr>
            <tr>
                <td>*Adres:</td> <td><input type="text" name="adres" value="<?php if(isset($_POST['registreer'])){echo $_POST['adres'];}?>" placeholder="Langeweg 123"/></td>
            </tr>
            <tr>
                <td>*Postcode:</td> <td><input type="text" name="postcode" value="<?php if(isset($_POST['registreer'])){echo $_POST['postcode'];}?>" placeholder="1234AA"/></td>
            </tr>
            <tr>
                <td>*Woonplaats:</td> <td><input type="text" name="woonplaats" value="<?php if(isset($_POST['registreer'])){echo $_POST['woonplaats'];}?>" placeholder="Amsterdam"/></td>
            </tr>
            <tr>
                <td>*Telefoonnummer:</td> <td><input type="text" name="telefoonnummer" value="<?php if(isset($_POST['registreer'])){echo $_POST['telefoonnummer'];}?>" placeholder="06-12345678"/></td>
            </tr>
            <tr>
                <td>*Email adres:</td> <td><input type="text" name="email" value="<?php if(isset($_POST['registreer'])){echo $_POST['email'];}?>" placeholder="henk@jamfactory.nl"/></td>
            </tr>
            <tr>
                <td>*Wachtwoord:</td> <td><input type="password" name="wachtwoord1"/></td>
            </tr>
            <tr>
                <td>*Wachtwoord herhalen:</td> <td><input type="password" name="wachtwoord2"/></td>
            </tr>
            </table>
            <div  style="text-align:center;">
             <input type="submit" name="registreer" value="Aanmelden"/>
            </div>
       </form>
     
        <h4> Velden met * zijn verplicht </h4>
</registratie> 
		</bodyfoutmelding>  
        </body>
</html>

<?php
//variabelen aanmaken
if(isset($_POST["registreer"])){
    $voornaam = $_POST["voornaam"];
    $tussenvoegsel = $_POST["tussenvoegsel"];
    $achternaam = $_POST["achternaam"];
    $adres = $_POST["adres"];
    $postcode = $_POST["postcode"];
    $woonplaats = $_POST["woonplaats"];
    $telefoonnummer = $_POST["telefoonnummer"];
    $email = $_POST["email"];
    $wachtwoord1 = $_POST["wachtwoord1"];
    $wachtwoord2 = $_POST["wachtwoord2"];
    $naam = naamplakken($voornaam, $tussenvoegsel, $achternaam);
}

// controle of alle verplichte velden zijn ingevuld
if(isset($_POST["registreer"])){
    if (controleervelden($naam, $telefoonnummer, $adres, $postcode, $woonplaats, $email, $wachtwoord1, $wachtwoord2) AND controleermail($email) == TRUE){
        $wachtwoord3 = md5($wachtwoord1);
       
        //query met alle ingevulde gegevens
        $sth = $dbh->prepare("INSERT INTO klant (naam, telefoonnummer, adres, postcode, woonplaats, email, wachtwoord, ingelogdStatus)
            VALUES (:naam, :telefoonnummer, :adres, :postcode, :woonplaats, :email, :wachtwoord1, 2)");
    
        //beveiliging
        $sth->bindValue(':naam', $naam, PDO::PARAM_STR);
        $sth->bindValue(':telefoonnummer', $telefoonnummer, PDO::PARAM_STR);
        $sth->bindValue(':adres', $adres, PDO::PARAM_STR);
        $sth->bindValue(':postcode', $postcode, PDO::PARAM_STR);
        $sth->bindValue(':woonplaats', $woonplaats, PDO::PARAM_STR);
        $sth->bindValue(':email', $email, PDO::PARAM_STR);
        $sth->bindValue(':wachtwoord1', $wachtwoord3, PDO::PARAM_STR);
    
        $sth->execute();
        $aantal=$sth->rowCount();
    
        if($aantal == 1){
            echo "<br>Registratie gelukt u kunt nu inloggen.";
        }else{
            echo "<br><br>Er is een fout op getreden neemt u a.u.b contact op met de beheerder van deze website.";
          }
     }
}
?>