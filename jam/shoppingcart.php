<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php

        include("connect-site.php");
        include("functions.php");
		include("menu.php");
        if ($_REQUEST) {
            if ($_REQUEST['command'] == 'delete' && $_REQUEST['pid'] > 0) {
                remove_product($_REQUEST['pid']);
            } else if ($_REQUEST['command'] == 'clear') {
                unset($_SESSION['cart']);
            } else if ($_REQUEST['command'] == 'update') {
                $max = count($_SESSION['cart']);
                for ($i = 0; $i < $max; $i++) {
                    $pid = $_SESSION['cart'][$i]['productid'];
                    $q = intval($_REQUEST['product' . $pid]);






                    if ($q < 1 or $q > 10) {
                        $msg = 'De hoeveelheid moet tussen de 1 en 10 zitten, anders kunt u contact opnemen met Gerard Luiken';
                    } elseif ($_SESSION['inlognaam']) {
                        $email = $_SESSION['inlognaam'];
                        $check = "SELECT SUM(aantal) FROM bestelregel WHERE orderID IN (SELECT orderID FROM bestelling WHERE klantID IN(SELECT klantID FROM klant WHERE email = :email ))";

                        $checking = $dbh->prepare($check);
                        $checking->bindValue(":email", $email, PDO::PARAM_STR);
                        $checking->execute();
                        $checked = $checking->fetchAll(PDO::FETCH_BOTH);
                        $customer = $checked[0];
                        $checked1 = $customer['SUM(aantal)'] + $q;

                        if (($checked1) > 10) {
                            $msg = "de bestelde  artikelen plus de artikelen die al zijn besteld zijn groter dan 10, neem contact op mer Gerard Luiken";
                        } elseif ($q > 0 && $q <= 10) {
                            $_SESSION['cart'][$i]['qty'] = $q;
                        }
                    }
                }
            }
        }
        ?>
        <script language="javascript">
            function del(pid) {
                if (confirm('Wilt u deze jam verwijderen?')) {
                    document.form1.pid.value = pid;
                    document.form1.command.value = 'delete';
                    document.form1.submit();
                }
            }
            function clear_cart() {
                if (confirm('Deze acties zullen uw gegevens van deze wagen verwijderen, doorgaan?')) {
                    document.form1.command.value = 'clear';
                    document.form1.submit();
                }
            }
            function update_cart() {
                document.form1.command.value = 'update';
                document.form1.submit();
            }


        </script>
    </head>

    <body>
<?php
if (isset($_SESSION['cart'])) {
    print "<a href=\"shoppingcart.php\">";
    $max = count($_SESSION['cart']);
    print "($max)winkelwagen";
    print "</a>";
}
?>
        <form name="form1" method="post">
            <input type="hidden" name="pid" />
            <input type="hidden" name="command" />
            <div style="margin:0px auto; width:600px;" >
                <div style="padding-bottom:10px">
                    <h1 align="center">Uw winkelwagen:</h1>
                    <input type="button" value="Doorgaan met winkelen" onclick="window.location = 'Catalogus_bekijken.php'" />
                </div>
<?php
if (isset($msg)) {
    print "<div style=\"color:#F00\"> $msg </div>";
}
?>
                <table border="0" cellpadding="5px" cellspacing="1px" style="font-family:Verdana, Geneva, sans-serif; font-size:11px; background-color:#E1E1E1" width="100%">
                <?php
                //kijken of er een winkelwagen bestaat
                if ($_SESSION) {
                    if (isset($_SESSION['cart'])) {

                        if (is_array($_SESSION['cart'])) {
                            echo '<tr bgcolor="grey" style="font-weight:bold"><td>ProductID</td><td>Naam</td><td>Stuksprijs</td><td>hoeveelheid</td><td>Totaalbedrag</td><td>Verwijderen</td></tr>';
                            $max = count($_SESSION['cart']);
                            for ($i = 0; $i < $max; $i++) {
                                $pid = $_SESSION['cart'][$i]['productid'];
                                $q = $_SESSION['cart'][$i]['qty'];
                                $pname = get_product_name($pid);
                                if ($q == 0)
                                    continue;
                                //winkelwagen laten zien
                                ?>

                                    <tr bgcolor="#FFFFFF"><td><?php echo $i + 1 ?></td><td><?php echo $pname ?></td>
                                        <td>$ <?php echo get_price($pid) ?></td>
                                        <td><input type="text" name="product<?php echo $pid ?>" value="<?php echo $q ?>" maxlength="2" size="2" /></td>                    
                                        <td>$ <?php echo get_price($pid) * $q ?></td>
                                        <td><a href="javascript:del(<?php echo $pid ?>)">verwijder</a></td></tr>
                <?php
            }
            ?>
                                <tr bgcolor="#FFFFFF"><td><b>Totaal: &euro;<?php echo get_order_total(); ?></b></td><td colspan="5" align="right"><input type="button" value="Doe de items weg" onclick="clear_cart();"/><input type="button" value="pas aan" onclick="update_cart();"/><input type="button" value="Plaats bestelling" onclick="window.location = 'billing.php';"/></td></tr>
                                <?php
                            }
                        }else {
                            echo "<tr bgColor='#FFFFFF'><td>Er is niks in uw winkelwagen</td>";
                        }
                    }
                    ?>
                </table>
            </div>
        </form>
    </body>
</html>