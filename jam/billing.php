<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
include("connect-site.php");
include("functions.php");
include("menu.php");
?>

<?php
if (!empty($_SESSION['cart'])) {
// variabelen aanmaken voor ongeregistreerde gebruiker
    if (!isset($_SESSION['inlognaam'])) {
        if ($_REQUEST) {
            if ($_REQUEST['command'] == 'update') {
                $name = $_REQUEST['name'];
                $email = $_REQUEST['email'];
                $address = $_REQUEST['address'];
                $phone = $_REQUEST['phone'];
                $postcode = $_REQUEST['postcode'];
                $woonplaats = $_REQUEST['woonplaats'];

                if (controleervelden($name, $phone, $address, $postcode, $woonplaats, $email) == FALSE) {
                    ?>

                    <script language="javascript">

                        alert('Alle velden moeten correct worden ingevuld!');
                    </script>;
                    <?php
                    print "<br/>Klik<a href=\"billing.php\"> hier</a> om het formulier weer in te vullen";
                    //header("Location: billing.php");
                    exit;
                } elseif (controleermail($email) == FALSE) {
                    ?>

                    <script language="javascript">

                        alert('Er is een bestaand email-adres ingevuld!');
                    </script>;
                    <?php
                    print "<br/>Klik<a href=\"billing.php\"> hier</a> om het formulier weer in te vullen";

                    exit;
                } else {

                    // ongereristreerde gebruiker is ingelogdStatus 1
                    $sql2 = "INSERT INTO klant(naam, telefoonnummer, adres, postcode, woonplaats, email, ingelogdStatus) VALUES(:name, :phone, :address, :postcode, :woonplaats, :email, 1)";

                    $sthd = $dbh->prepare($sql2);

                    $sthd->bindValue(":name", $name, PDO::PARAM_STR);
                    $sthd->bindValue(":email", $email, PDO::PARAM_STR);
                    $sthd->bindValue(":address", $address, PDO::PARAM_STR);
                    $sthd->bindValue(":phone", $phone, PDO::PARAM_STR);
                    $sthd->bindValue(":postcode", $postcode, PDO::PARAM_STR);
                    $sthd->bindValue(":woonplaats", $woonplaats, PDO::PARAM_STR);


                    $sthd->execute();
                }
            }
        }
    }

//		$result=mysql_query("insert into customers values('','$name','$email','$address','$phone')");
//		$customerid=mysql_insert_id();
//		$date=date('Y-m-d');
//		$result=mysql_query("insert into orders values('','$date','$customerid')");
//        $orderid = mysql_insert_id();
    if (isset($_POST['bestel!'])) {
        if (isset($_SESSION['inlognaam'])) {
            $email1 = $_SESSION['inlognaam'];
        } else {
            $email1 = $email;
        }
        $preKlantID = "SELECT klantID FROM klant WHERE email = :inlognaam";
        $klantID = $dbh->prepare($preKlantID);
        $klantID->bindValue(":inlognaam", $email1, PDO::PARAM_STR);
        $klantID->execute();

        $klant = $klantID->fetchAll(PDO::FETCH_BOTH);
        $customer = $klant[0];


        $customerr = $customer['klantID'];



        //insert in bestelregel
        $max = count($_SESSION['cart']);
        $date = date("Y-m-d H:i:s");
        $prep = "INSERT INTO bestelling(klantID,status,datum_bestelt) VALUES (:customer, 1,:date)";

        $prep1 = "SELECT MAX(orderID) FROM bestelling";

        $prepp = $dbh->prepare($prep);
        $prepp1 = $dbh->prepare($prep1);
        $prepp->bindValue(":date", $date, PDO::PARAM_STR);
        $prepp->bindValue(":customer", $customerr, PDO::PARAM_STR);

        $prepp->execute();
        $prepp1->execute();

        $result = $prepp1->fetchAll(PDO::FETCH_BOTH);




        for ($i = 0; $i < $max; $i++) {
            foreach ($result as $nr) {
                $id = $nr['MAX(orderID)'];
                $pid = $_SESSION['cart'][$i]['productid'];
                $q = $_SESSION['cart'][$i]['qty'];
                //   $price = get_price($pid);

                $std = "INSERT INTO bestelregel(orderID, productID, aantal, idProduct) values (:id, :pid, :q, 1)";


                $std2 = $dbh->prepare($std);


                //  $std2->bindValue(":orderid", $orderid, PDO::PARAM_STR);
                $std2->bindValue(":pid", $pid, PDO::PARAM_STR);
                $std2->bindValue(":q", $q, PDO::PARAM_STR);
                //   $std2->bindValue(":price", $price, PDO::PARAM_STR);
                $std2->bindValue(":id", $id, PDO::PARAM_STR);
                $std2->execute();
//            mysql_query("insert into order_detail values ($orderid,$pid,$q,$price)");
            }
        }// verzend de mail
        $mailto = "test@testjamfactory.3space.info";
        $subject = "Nieuwe bestelling";
        $content = "Er is een nieuwe bestelling geplaatst, er is iets besteld. ga naar statusInzien om bestelling te zien";
        if (!isset($_SESSION['inlognaam'])) {
            $contentHeader = $email;
        } else {
            $contentHeader = $_SESSION['inlognaam'];
        }
        $header = $contentHeader;
        mail($mailto, $subject, $content, $header);


//        die('Thank You! your order has been placed!');


        header('Location: bedankt.php');
    } elseif (!isset($_POST['bestel!'])) {
        ?>


        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <script language="javascript">
            function validate() {
                var f = document.form1;
                if (f.name.value == '') {
                    alert('De naam is verplicht');
                    f.name.focus();
                    return false;
                }
                f.command.value = 'update';
                f.submit();
            }
        </script>
        </head>

        <body>
            <form name="form1" onsubmit="return validate()" action="billing.php" method="POST">
                <input type="hidden" name="command" />
                <div align="center">
                    <h1 align="center">Bestellingsoverzicht</h1>
                    <table border="0" cellpadding="2px">
                        <tr><td>Order Total:</td><td><?php echo get_order_total() ?></td></tr>


        <?php
        if (!isset($_SESSION['inlognaam'])) {
            ?>
                            <tr><td>Naam</td><td><input type="text" name="name" /></td></tr>
                            <tr><td>Adres:</td><td><input type="text" name="address"/></td></tr>
                            <tr><td>Email:</td><td><input type="text" name="email" /></td></tr>
                            <tr><td>Telefoonnummer:</td><td><input type="text" name="phone"/></td></tr>
                            <tr><td>postcode:</td><td><input type="text" name="postcode" /></td></tr>
                            <tr><td>woonplaats:</td><td><input type="text" name="woonplaats"  /></td></tr>

            <?php
        }
        ?>
                        <tr><td>&nbsp;</td><td><input type="submit" value="Plaats bestelling" name="bestel!" /></td></tr>
                    </table>
                </div>
            </form>


        <?php
    }
} else {
    print "u moet nog iets bestellen";
    print "<br>";
    print "u word over 5 seconden doorgestuurd naar de productenpagina";
    ?>

        <script language="javascript">
                        setTimeout(function() {
                            window.location.href = "catalogus_bekijken.php"; //Redirect deze pagina naar: catalogus_bekijken.php
                        }, 5000); //deze functie vertraagt het met 5 seconden
        </script>

    <?php
}
?>
</body>
</html>