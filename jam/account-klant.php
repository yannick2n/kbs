<?php
session_start();
$_SESSION['inlognaam'];
include 'menu.php';
include 'connect-site.php';
?>

<?php
$actie = "";
$ids = "";
$geselecteerdAccount = array();

//sessie beheerder//

if (isset($_SESSION['inlognaam'])) {
    $email1 = $_SESSION['inlognaam'];
    $preKlantID = "SELECT ingelogdStatus FROM klant WHERE email = :inlognaam";
    $klantID = $dbh->prepare($preKlantID);
    $klantID->bindValue(":inlognaam", $email1, PDO::PARAM_STR);
    $klantID->execute();
    foreach ($klantID as $klant) {
        if ($klant['ingelogdStatus'] >= 1) {

            if (isset($_SESSION['inlognaam'])) {
                $email = $_SESSION['inlognaam'];
            }

            $actie = "";
            $ids = "";
            $geselecteerdAccount = array();
            if (isset($_POST['ids'])) {
                if (isset($_POST['actie'])) {
                    $actie = $_POST['actie'];

//Verwijderen van geselecteerde klantregistratie//

                    if ($actie == "verwijderen") {

                        $ids = $_POST['ids'][0];
                        //  verwijderen
                        $st = "DELETE FROM klant WHERE klantID =:ids ";

                        $sth = $dbh->prepare($st);
                        $sth->bindValue(':ids', $ids, PDO::PARAM_STR);


                        $sth->execute();
                    }
		}
//Wijzigen van geselecteerde klantregistratie//	
                    if ($actie == "wijzigen") {
                        //$ids = $_POST['klantID'];

                        $nr = $_POST['ids'][0]; {
//Ophalen van bestaande registratiegegevens en plaatsen in wijzigvelden//							
                            $sql3 = $dbh->prepare("
	SELECT naam, telefoonnummer, adres, postcode, woonplaats
        FROM klant WHERE klantID=$nr");
                            $sql3->execute();
                            //$geselecteerdAccount = $sql3->fetchAll(PDO::FETCH_BOTH);
                            $result2 = $sql3->fetchAll(PDO::FETCH_BOTH);
                            foreach ($result2 as $geselecteerdAccount)
                                ;
                        }
                    }
                }
//Opslaan gewijzigde gegevens  waneer op opslaan wordt geklikt//			
                if ($actie == "opslaan") {
                    $klantID = $_POST['klantID'];
                    $naam = $_POST['naam'];
                    $telefoonnummer = $_POST['telefoonnummer'];
                    $adres = $_POST['adres'];
                    $postcode = $_POST['postcode'];
                    $woonplaats = $_POST['woonplaats'];
                    $email = $_POST['email'];

                    $std = "UPDATE klant SET naam='$naam',telefoonnummer='$telefoonnummer',adres='$adres',postcode='$postcode',woonplaats='$woonplaats'
                        WHERE klantID=$klantID";
                    $stdd = $dbh->prepare($std);

                    $stdd->execute();
                }
            }
//Weergeven alle klantregistraties//		
            $sth = $dbh->prepare("SELECT * FROM klant WHERE email = '" . $_SESSION['inlognaam'] . "'");
            $sth->execute();
            $accountlijst = $sth->fetchAll(PDO::FETCH_BOTH);
            ?>
            <article multilinks-noscroll="true" id="content" class="clearfix">
                <h1>Account beheer</h1>

                <form action="./account-klant.php" method="POST">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <td>Selectie</td>
                                <td>Naam</td>
                                <td>Telefoonnummer</td>
                                <td>Adres</td>
                                <td>Postcode</td>
                                <td>Woonplaats</td>
                                <td>E-mail</td>
                            </tr>
                        </thead>

                        <?php
//Stop alle gegevens in een tabel//
                        foreach ($accountlijst as $accountdetails) {
                            print ("
	
            <tr> 
				<td> <input style= inline type=\"radio\" name=\"ids[]\" value=\"$accountdetails[klantID]\"></td>
			    <td>" . ($accountdetails['naam']) . "</td>
				<td>" . ($accountdetails['telefoonnummer']) . "</td>
			    <td>" . ($accountdetails['adres']) . "</td>
    			<td>" . ($accountdetails['postcode']) . "</td>
				<td>" . ($accountdetails['woonplaats']) . "</td>
			    <td>" . ($accountdetails['email']) . "</td>
			</tr>	
		 ");
                        }
                        ?>	</table>
                    <table>
                        <?php
//Formulier met de bestaande gegevens en een opslaan knop tonen.//			 					
                        if ($actie == "wijzigen") {


                            print("<input type=\"hidden\" name=\"klantID\" value=\"$nr\"/>");
                            print("Naam:</br><input type=\"text\" value=\"" . $geselecteerdAccount['naam'] . "\" name=\"naam\"/></br>");
                            print("Telefoonnummer:</br><input type=\"text\" value=\"" . $geselecteerdAccount['telefoonnummer'] . "\" name=\"telefoonnummer\"/></br>");
                            print("Adres:</br><input type=\"text\" value=\"" . $geselecteerdAccount['adres'] . "\" name=\"adres\"/></br>");
                            print("Postcode:</br><input type=\"text\" value=\"" . $geselecteerdAccount['postcode'] . "\" name=\"postcode\"/></br>");
                            print("Woonplaats:</br><input type=\"text\" value=\"" . $geselecteerdAccount['woonplaats'] . "\"  name=\"woonplaats\"/></br>");
                            print("<input type='submit' value='opslaan' name='actie'><br>");
                            print 'Herkies uw selectie om de wijziging te bevestigen';
                        }
                    }
                    ?>
                </table>
                </br>
                <div  style="text-align:center;">
                    <input class="wijzigen" type="submit" value="wijzigen" name="actie"/>
                    <input class="verwijderen" type="submit" value="verwijderen" name="actie"/>
                </div>
            </form>
        </article>
    <?php
    
} else {
    print "Login met uw beheerders account om toegang tot deze pagina te krijgen";
}
?>
</body>
</html>