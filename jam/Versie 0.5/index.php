<!DOCTYPE html>
<?php session_start();?>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>JAMFACTORY</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="Jamfactory_bestanden/normalize.css" type="text/css">
		<link rel="stylesheet" href="Jamfactory_bestanden/main.css" type="text/css">
        <link rel="stylesheet" href="Jamfactory_bestanden/inlog.css" type="text/css">
		<link rel="icon" href="Jamfactory_bestanden/animated_favicon.gif" type="image/gif" >

        <script type="text/javascript" src="Jamfactory_bestanden/modernizr-2.js"></script>
		<script type="text/javascript" src="Jamfactory_bestanden/jquery.js"></script>
		<script type="text/javascript" src="Jamfactory_bestanden/waypoints.js"></script>
		<script type="text/javascript" src="Jamfactory_bestanden/jquery_003.js"></script>
        <script type="text/javascript" src="Jamfactory_bestanden/ie-placeholder-fix.js"></script>
		<script type="text/javascript" src="Jamfactory_bestanden/jquery_002.js"></script>
		<script type="text/javascript" src="Jamfactory_bestanden/main.js"></script>
		<script type="text/javascript" src="Jamfactory_bestanden/jquery_004.js"></script>
        <script type="text/javascript" src="Jamfactory_bestanden/bootstrap-dropdown.js"></script>
	</head>
    <body style="-moz-user-select: text;" class="home">
	<header multilinks-noscroll="true">
			<nav multilinks-noscroll="true" id="main">
				<ul multilinks-noscroll="true" id="main-nav">
					<li multilinks-noscroll="true" class="first">				<a multilinks-noscroll="true" class="scroll" href="#Thuis">Home</a></li>
					<li multilinks-noscroll="true" class="">					<a multilinks-noscroll="true"  href="/catalogus_bekijken.php">Catalogus</a></li>
					<li multilinks-noscroll="true" class="">					<a multilinks-noscroll="true" class="scroll" href="#Reviews">Reviews</a></li>
                    <li multilinks-noscroll="true" class="">			        <a multilinks-noscroll="true" class="" href="/cursus_overzicht.php">Cursus</a></li>
					<li multilinks-noscroll="true" class="last active">			<a multilinks-noscroll="true" class="" href="/contact-form.php">Contact</a></li>
				<?php
                    if(isset($_SESSION['status']) AND $_SESSION['status'] == 3){
                        echo '<li multilinks-noscroll="true" class="">					<a multilinks-noscroll="true"  href="/beheer.php">Beheer</a></li>';
                    }
                    
                    ?>
                </ul>
                <ul multilinks-noscroll="true" id="secondary-nav">
                    <li multilinks-noscroll="true" class="dropdown">	 
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Account<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/contact-form.php">Wachtwoord vergeten</a></li>                            
                            <?php
                            if(isset($_SESSION['inlognaam'])){
                                echo "<li><a href='/loguit.php'>Uitloggen</a></li>";
                            }else{
                                echo '<li><a href="/registreren.php">Registreer</a></li>';
                            }
                            ?>
                        </ul>
                        <?php
                        if (!isset($_SESSION['inlognaam'])) {
                            ?>
                            <form action="controleerwachtwoord.php" method="POST" class="navbar-form pull-right">
                                <input class="span2" type="text" placeholder="email" name="email">
                                <input class="span2" type="password" name="wachtwoord" placeholder="Wachtwoord">
                                <button type="submit" name="login" class="btn btn-primary">Inloggen</button>
                            </form>
                            <?php
                        } else {
                            $naam = $_SESSION['inlognaam'];
                            echo "<li multilinks-noscroll='true' class=''><a href='http://testjamfactory.3space.info/account-klant.php' multilinks-noscroll='true'>Ingelogd als: $naam</a></li>";
                        }
                        ?>
                    </li>
                </ul>			
            </nav>
        </header>
		<a multilinks-noscroll="true" href="#Thuis" class="scroll show" id="to-top">top</a>		
		<div multilinks-noscroll="true" id="content" role="main">			
<!-- THUIS -->
			<article multilinks-noscroll="true" id="home">
				<a multilinks-noscroll="true" href="#Thuis" class="scroll">Thuis</a>
			</article>							
<!-- ONZE JAM -->
			<article multilinks-noscroll="true" id="Thuis" class="clearfix">
				<h1>Welkom op jamfactory.nl</h1>
				<div multilinks-noscroll="true" class="container clearfix">
					<span class="split">
						<div class="one-fourth first">
							<h3>Vers Fruit</h3>
							<div class="icon"><img src="Jamfactory_bestanden/fruit.png" class="svg icon" height="64" width="72"></div>
							<p>Heb je een favoriet soort fruit</p>
							<p>Kies de jam die bij je past.</p>
						</div>
						<div class="one-fourth">
							<h3>Puur Natuur</h3>
							<div class="icon"><img src="Jamfactory_bestanden/natuur.svg" class="svg icon" height="64" width="72"></div>
							<p>Onze producten bestaan uit 100% natuurlijk fruit voorgeselecteerd op de beste kwaliteit.</p>
						</div>
					</span>
					<span multilinks-noscroll="true" class="split">
						<div multilinks-noscroll="true" class="one-fourth third">
							<h3>Geen toegevoegde kleurstoffen</h3>
							<div class="icon"><img src="Jamfactory_bestanden/kleur.png" class="svg icon" height="64" width="72"></div>
							<p>Onze producten zijn 100% natuur dus geen kunstmatige kleuren alles wat je ziet is de kleur van het fruit,</p>
						</div>
						<div class="one-fourth">
							<h3>Geen conserveermiddelen</h3>
							<div class="icon"><img src="Jamfactory_bestanden/conserveer.svg" class="svg icon" height="64" width="72"></div>
							<p>Door dat wij voor 100% natuur gaan zijn onze producten vrij van E-nummers.</p>
						</div>
					</span>
				</div>
				<p multilinks-noscroll="true" class="call-to-action"><a multilinks-noscroll="true" href="/Catalogus_bekijken.php" class="button">Bekijk catalogus</a></p>
			</article>
			<div class="hr swords"></div>					
<!-- JAMFACTORY -->
			<article multilinks-noscroll="true" id="Jam" class="clearfix slides-viewport" data-slide-controls="nextback" data-scroll-top="true">
				<h1>Onze Jam</h1>
				<div multilinks-noscroll="true" data-current-slide="0" class="slides-container two-slides"><a class="previous-slide slide-control hidden" data-type="prev" href="#"></a>
				
					<div multilinks-noscroll="true" class="primary-layer slide" data-name="Fruit">
						<div class="summary">
							<p>Fruitig, 100% puur natuur en geen toegevoegde kleurstoffen of conserveermiddelen.</p>
						</div>
						<div multilinks-noscroll="true" id="blade-matrix" class="container clearfix image-matrix" data-buynow="Bekijk catalogus">
							<div multilinks-noscroll="true" class="one-half first">
							  <a multilinks-noscroll="true" href="/Catalogus_bekijken.php?zoek=aardbei&zoeken=Zoek" 		class="fb first long"    data-title="Vers Fruit"><div class="overlay"></div><img src="Jamfactory_bestanden/1.jpg" alt="Fruit" class="long" height="280" width="230"></a>
							  <a multilinks-noscroll="true" href="/Catalogus_bekijken.php?zoek=samenstelling&zoeken=Zoek" 	class="fb"  			 data-title="Vers Fruit"><div class="overlay"></div><img src="Jamfactory_bestanden/2.jpg" alt="Fruit" height="230" width="230"></a>
							  <a multilinks-noscroll="true" href="/Catalogus_bekijken.php?zoek=peer&zoeken=Zoek" 			class="fb"               data-title="Vers Fruit"><div class="overlay"></div><img src="Jamfactory_bestanden/3.jpg" alt="Fruit" height="230" width="230"></a>
							  <a multilinks-noscroll="true" href="/Catalogus_bekijken.php?zoek=banaan&zoeken=Zoek" 			class="fb first wide"    data-title="Vers Fruit"><div class="overlay"></div><img src="Jamfactory_bestanden/4.jpg" alt="Fruit" class="wide" height="198" width="438"></a>			
                          </div>						
							<div multilinks-noscroll="true" class="one-half">
							  <a multilinks-noscroll="true" href="/Catalogus_bekijken.php?zoek=kersen&zoeken=Zoek" 			class="fb first wide"    data-title="Vers Fruit"><div class="overlay"></div><img src="Jamfactory_bestanden/5.jpg" alt="Fruit" class="wide" height="258" width="218"></a>
							  <a multilinks-noscroll="true" href="/Catalogus_bekijken.php?zoek=aardbei&zoeken=Zoek" 		class="fb first long"    data-title="Vers Fruit"><div class="overlay"></div><img src="Jamfactory_bestanden/6.jpg" alt="Fruit" class="long" height="212" width="230"></a>
							  <a multilinks-noscroll="true" href="/Catalogus_bekijken.php?zoek=sinaasappel&zoeken=Zoek" 			class="fb"  			 data-title="Vers Fruit"><div class="overlay"></div><img src="Jamfactory_bestanden/8.jpg" alt="Fruit" height="190" width="242"></a>
                              <a multilinks-noscroll="true" href="/Catalogus_bekijken.php?zoek=kiwi&zoeken=Zoek" 	class="fb"		         data-title="Vers Fruit"><div class="overlay"></div><img src="Jamfactory_bestanden/7.jpg" alt="Fruit" height="230" width="230"></a>
						  </div>
						</div>
					</div>
				<a multilinks-noscroll="true" class="next-slide slide-control" data-type="next" href="#"></a></div>
				<p multilinks-noscroll="true" class="call-to-action"><a multilinks-noscroll="true" href="/Catalogus_bekijken.php" class="button">Bekijk catalogus</a></p>
			</article>
			<div class="hr crown"></div>	
<!-- Reviews -->
			<article multilinks-noscroll="true" id="Reviews" class="slides-viewport" data-slide-controls="nextback" data-scroll-top="true">
				<h1>reviews</h1>
				<p class="summary">Jamfactory ervaringen</p>
				<div multilinks-noscroll="true" data-current-slide="0" class="slides-container two-slides"><a class="previous-slide slide-control hidden" data-type="prev" href="#"></a>
					
					<div class="primary-layer slide">
						<div class="container clearfix">
							<div class="one-third first">
								<img src="Jamfactory_bestanden/jam-crown.svg" class="crown svg" height="12" width="34">
								<p><a href="/review_plaatsen.php"><img src="Jamfactory_bestanden/peer.jpg" height="120" width="120"></a></p>
								<h2>Perenjam</h2>
								<img src="Jamfactory_bestanden/review-facebook.svg" class="svg" alt="review" height="66" width="61">
								<div class="left-align review-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pretium at massa eget malesuada. Phasellus suscipit pharetra risus.</p>
								</div>
							</div>
							<div class="one-third">
								<img src="Jamfactory_bestanden/jam-crown.svg" class="crown svg" height="12" width="34">
								<p><img src="Jamfactory_bestanden/kers.jpg" height="120" width="120"></p>
								<h2>Kersenjam</h2>
								<img src="Jamfactory_bestanden/review-email.svg" class="svg" alt="review" height="66" width="61">
								<div class="left-align review-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
								</div>					
							</div>
							<div class="one-third">
								<img src="Jamfactory_bestanden/jam-crown.svg" class="crown svg" height="12" width="34">
								<p><a href="/reviews_overzicht_g_gebruiker.php"><img src="Jamfactory_bestanden/banana.jpg" height="120" width="120"></a></p>
								<h2>Bananajam</h2>
								<img src="Jamfactory_bestanden/review-facebook.svg" class="svg" alt="review" height="66" width="61">
								<div class="left-align review-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pretium at massa eget malesuada. Phasellus suscipit pharetra risus, in tincidunt metus.</p>
								</div>	                               
							</div><p multilinks-noscroll="true" class="call-to-action"><a multilinks-noscroll="true" href="/Catalogus_bekijken.php" class="button">Bekijk catalogus</a></p>
						</div>
					</div>
				</div>
			</div>
			<div class="hr shield"></div>            
<!-- FOOTER -->
			<footer>
				<div class="container">
					<ul class="footer-nav">
						<li><a href="http://testjamfactory.3space.info/contact-form.php">Contact</a></li>
					</ul>
				</div>
			</footer>
<!-- /FOOTER -->
</html>