<!DOCTYPE html>
<?php include 'menu.php'; ?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <h1> Product toevoegen </h1>
            <form action="product_toevoegen.php" method="POST">
                <table>
            <tr>
                <td>Naam:</td> <td><input type="text" name="naam" value="<?php if(isset($_POST['plaats'])){echo $_POST['naam'];}?>" placeholder="Bananen Jam"/></td>
            </tr>
            <tr>
                <td>Soort:</td> <td><input type="text" name="soort" value="<?php if(isset($_POST['plaats'])){echo $_POST['soort'];}?>" placeholder="Tropisch"/></td>
            </tr>
            <tr>
                <td>Stuksprijs:</td> <td><input type="text" name="stuksprijs" value="<?php if(isset($_POST['plaats'])){echo $_POST['stuksprijs'];}?>" placeholder="3.95"/></td>
            </tr>
            <tr>
                <td>Omschrijving:</td> <td><input type="text" name="omschrijving" value="<?php if(isset($_POST['plaats'])){echo $_POST['omschrijving'];}?>" placeholder="Lekkere jam"/></td>
            </tr>
            <tr>
                <td>Plaatje:</td> <td><input type="text" name="plaatje" value="<?php if(isset($_POST['plaats'])){echo $_POST['plaatje'];}?>" placeholder="plaatje.nl/plaatje.jpg"/></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" name="plaats" value="Voeg toe"/></td>
            </tr>
            </table>
        </form>
    </body>
</html>
<?php
include 'functions.php';
include 'connect-site.php';

//variabelen aanmaken
if(isset($_POST["plaats"])){
    $naam = $_POST["naam"];
    $soort = $_POST["soort"];
    $stuksprijs = $_POST["stuksprijs"];
    $omschrijving = $_POST["omschrijving"];
    $plaatje = $_POST["plaatje"];
}

// controle of alle verplichte velden zijn ingevuld
if(isset($_POST["plaats"])){
    if (controleerproduct($naam, $soort, $stuksprijs, $omschrijving, $plaatje)== TRUE){
        
        //query met alle ingevulde gegevens
        $sth = $dbh->prepare("INSERT INTO product (naam, soort, stuksprijs, omschrijving, plaatje)
            VALUES (:naam, :soort, :stuksprijs, :omschrijving, :plaatje)");
    
        //beveiliging
        $sth->bindValue(':naam', $naam, PDO::PARAM_STR);
        $sth->bindValue(':soort', $soort, PDO::PARAM_STR);
        $sth->bindValue(':stuksprijs', $stuksprijs, PDO::PARAM_STR);
        $sth->bindValue(':omschrijving', $omschrijving, PDO::PARAM_STR);
        $sth->bindValue(':plaatje', $plaatje, PDO::PARAM_STR);
    
        $sth->execute();
        $aantal=$sth->rowCount();
    
        if($aantal == 1){
            echo "Product toegevoegd!";
        }else{
            echo "<br><br>Er is iets mis gegaan";
          }
     }
}
?>