<!DOCTYPE html>
<?php session_start();?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>JAMFACTORY</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="stylesheet" href="Jamfactory_bestanden/normalize.css" type="text/css">
        <link rel="stylesheet" href="Jamfactory_bestanden/main.css" type="text/css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="icon" href="Jamfactory_bestanden/animated_favicon.gif" type="image/gif" >

        <script src="Jamfactory_bestanden/modernizr-2.js"></script>

        <script type="text/javascript" src="Jamfactory_bestanden/jquery.js"></script>
        <script type="text/javascript" src="Jamfactory_bestanden/waypoints.js"></script>
        <script type="text/javascript" src="Jamfactory_bestanden/jquery_003.js"></script>
        <script type="text/javascript" src="Jamfactory_bestanden/ie-placeholder-fix.js"></script>
        <script type="text/javascript" src="Jamfactory_bestanden/jquery_002.js"></script>
        <script type="text/javascript" src="Jamfactory_bestanden/main.js"></script>
        <script type="text/javascript" src="Jamfactory_bestanden/jquery_004.js"></script>
        <script type="text/javascript" src="Jamfactory_bestanden/bootstrap-dropdown.js"></script>

    </head>
    <body style="-moz-user-select: text;" class="home">

        <header multilinks-noscroll="true">
            <nav multilinks-noscroll="true" id="main">
                <ul multilinks-noscroll="true" id="main-nav">
                    <li multilinks-noscroll="true" class="first">				<a multilinks-noscroll="true"  href="http://testjamfactory.3space.info/#Thuis">Home</a></li>
                    <li multilinks-noscroll="true" class="">					<a multilinks-noscroll="true"  href="http://testjamfactory.3space.info/catalogus_bekijken.php">Catalogus</a></li>
                    <li multilinks-noscroll="true" class="">					<a multilinks-noscroll="true"  href="http://testjamfactory.3space.info/reviews_overzicht.php">Reviews</a></li>
                    <li multilinks-noscroll="true" class="">					<a multilinks-noscroll="true"  href="http://testjamfactory.3space.info/cursus_overzicht.php">Cursus</a></li>
                    <li multilinks-noscroll="true" class="">					<a multilinks-noscroll="true"  href="http://testjamfactory.3space.info/contact-form.php">Contact</a></li>
                    <?php
                    if(isset($_SESSION['status']) AND $_SESSION['status'] == 3){
                        echo '<li multilinks-noscroll="true" class="">					<a multilinks-noscroll="true"  href="/beheer.php">Beheer</a></li>';
                    }
                    
                    ?>
                </ul>
                <ul multilinks-noscroll="true" id="secondary-nav">
                    <li multilinks-noscroll="true" class="dropdown">	 
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Account<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/contact-form.php">Wachtwoord vergeten</a></li>
                            
                            <?php
                            if(isset($_SESSION['inlognaam'])){
                                echo "<li><a href='/loguit.php'>Uitloggen</a></li>";
                            }else{
                                echo '<li><a href="/registreren.php">Registreer</a></li>';
                            }
                            ?>
                        </ul>
                        <?php
                        if (!isset($_SESSION['inlognaam'])) {
                            ?>
                            <form action="controleerwachtwoord.php" method="POST" class="navbar-form pull-right">
                                <input class="span2" type="text" placeholder="email" name="email">
                                <input class="span2" type="password" name="wachtwoord" placeholder="Wachtwoord">
                                <button type="submit" name="login" class="btn btn-primary">Inloggen</button>
                            </form>
                            <?php
                        } else {
                            $naam = $_SESSION['inlognaam'];
                            echo "<li multilinks-noscroll='true' class=''><a href='http://testjamfactory.3space.info/account-klant.php' multilinks-noscroll='true'>Ingelogd als: $naam</a></li>";
                        }
                        ?>
                    </li>
                </ul>			
            </nav>
        </header>
        <a multilinks-noscroll="true" href="" id="to-top">top</a>		

        <div multilinks-noscroll="true" id="content" role="main">