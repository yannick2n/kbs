<?php

// controleer of er een formulier is ontvangen - zo niet stuur terug naar contact form
if (!isset($_POST["save"]) || $_POST["save"] != "contact") {
    header("Location: contact-form.php"); exit;
}
	
// verkrijg de verzonden data
$name = $_POST["contact_name"];
$email_address = $_POST["contact_email"];
$message = $_POST["contact_message"];
	
// controleer naam
if (empty ($name))
    $error = "Naam is verplicht.";
// controleer of e-mail is ingevuld
elseif (empty ($email_address)) 
    $error = "E-mailadres is verplicht.";
// controleer op een valide e-mailadres
elseif (!preg_match("/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/", $email_address))
    $error = "E-mailadres is niet geldig.";
// controleer of er een bericht is ingevuld
elseif (empty ($message))
    $error = "Bericht is verplicht.";
		
// controleer of er iets mist - zo ja, stuur terug naar invoer form
if (isset($error)) {
    header("Location: contact-form.php?e=".urlencode($error)); exit;
}
		
// dit krijg je te zien in de e-mail
$email_content = "Naam: $name\n";
$email_content .= "E-mailadres: $email_address\n";
$email_content .= "Bericht:\n\n$message";

// headers content-type
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

// headers

$headers= 'From: '.$name.'<'.contactbericht.'>'. "\r\n".
		  'Reply-To: '.$email_address."\r\n";


// stuur mail naar
mail ("test@testjamfactory.3space.info", "Website Contact Bericht", $email_content,$headers);
	
// stuur terug naar begin form met bevestiging
header("Location: contact-form.php?s=".urlencode("Bedankt voor je bericht.")); exit;

?>
