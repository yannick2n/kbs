<?php


//plakt voornaam tussen voegsel aan elkaar
function naamplakken($voornaam, $tussenvoegsel, $achternaam){
    return "$voornaam $tussenvoegsel $achternaam";
}


//Controleerd of alle velden zijn ingevuld en of de wachtwoorden aan de gestelde eisen voldoen
function controleervelden($naam, $telefoonnummer, $adres, $postcode, $woonplaats, $email, $wachtwoord1, $wachtwoord2){
    if(preg_match("#[a-z]+#", $telefoonnummer) OR strlen($telefoonnummer)>20 OR strlen($telefoonnummer)<8){
        echo "<br><br>Ongeldig telefoonnummer ingevuld";
        return FALSE;
    }elseif(preg_match("#[0-9]+#", $naam) OR strlen($naam)>50 OR preg_match('/[^a-z\s-]/i', $naam)){
        echo "<br><br> Ongeldige naam ingevuld";
        return FALSE;
    }elseif(strlen($adres)>30 OR strlen($adres)<8 OR !preg_match('/[^a-z\s-]/i', $adres)){
        echo "<br><br>Ongeldig adres ingevuld";
        return FALSE;
    }elseif(strlen($postcode) < 5 OR strlen($postcode) > 8 OR  !preg_match('/[^a-z\s-]/i', $postcode)){
        echo "<br><br>Ongeldige postcode ingevuld";
        return FALSE;
    }elseif(empty($woonplaats) OR strlen($postcode) > 30){
        return FALSE;
        echo "<br><br>Woonplaats niet ingevuld";
    }elseif(empty($email)){
        return FALSE;
        echo "<br><br>Email niet ingevuld";
    }elseif($wachtwoord1 != $wachtwoord2 OR empty($wachtwoord1)){
        echo "<br><br>Ingevulde wachtwoorden komen niet overeen";
        return FALSE;
    }elseif(strlen($wachtwoord1)<6 OR strlen($wachtwoord1)>20 OR !preg_match("#[0-9]+#", $wachtwoord1) OR !preg_match("#[a-z]+#", $wachtwoord1)) {
        echo"<br><br>Wachtwoord voldoet niet aan de gestelde eisen: minimaal 6 tekens waarvan minimaal 1 cijfer.";
        return FALSE;
    }else{
        return TRUE;
    }
}

//controleerd of het ingevulde email adres uniek is en of de ingevulde tekens op dat van een email adres lijken
function controleermail ($email){
           $user = "u834334437_jam";
        $pass = "xtEBUzamFJ";

        $dbh = new PDO(
                        'mysql:host=mysql.5freehosting.com;dbname=u834334437_jam', $user, $pass);

    $sth = $dbh->prepare("SELECT * FROM klant WHERE email = :email");
    $sth->bindValue(':email', $email, PDO::PARAM_STR);
    
    $sth->execute();

    $sth->execute();
    $aantal=$sth->rowCount();
    
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        echo "<br><br>Onjuist email adres ingevuld";
        return FALSE;
    }elseif ($aantal != 0){
        echo "<br><br>Er bestaat al een account met het opgegeven e-mailadres.";
        return FALSE;
    }else{
        return TRUE;
    }
}


//controleert de velden die ingevuld moeten worden bij product_toevoegen.php

function controleerproduct($naam, $soort, $stuksprijs, $omschrijving, $plaatje){
    if(strlen($naam)>100 OR empty($naam)){
        echo "<br><br>Naam te lang of leeg";
        return FALSE;
    }elseif(strlen($soort)>100 OR empty($soort)){
        echo "<br><br> Soort te lang of leeg";
        return FALSE;
    }elseif(preg_match("/^([a-z0-9\s]+_?[a-z0-9]+[a-z0-9\s]+)+$/i", $stuksprijs) OR empty($stuksprijs)){
        echo "<br><br>Ongeldige prijs ingevoerd. Gebruik een punt i.p.v. komma";
        return FALSE;
    }elseif(strlen($omschrijving)>500 OR empty($omschrijving)){
        echo "Omschrijving leeg of te lang (max 500 letters)";
        return FALSE;
    }elseif(empty($plaatje)){
        echo "Plaatje niet ingevuld";
        return FALSE;
    }else{
        return TRUE;
    }
}

function get_product_name($pid) {
    $user = "u834334437_jam";
    $pass = "xtEBUzamFJ";
    $dbh = new PDO('mysql:host=mysql.5freehosting.com;dbname=u834334437_jam', $user, $pass);


    $result = "select naam from product where productID=$pid";
    $result1 = $dbh->prepare($result);
    $result1->execute();
    foreach ($result1 as $row) {
        return $row['naam'];
    }
}

function get_price($pid) {
    $user = "u834334437_jam";
    $pass = "xtEBUzamFJ";
    $dbh = new PDO('mysql:host=mysql.5freehosting.com;dbname=u834334437_jam', $user, $pass);

    $result = "select stuksprijs from product where productID=$pid";
    $result1 = $dbh->prepare($result);
    $result1->execute();
    foreach ($result1 as $row) {
//		$result=mysql_query("select stuksprijs from product where productID=$pid") or die("select naam from product where productID=$pid"."<br/><br/>".mysql_error());
//		$row=mysql_fetch_array($result);
        return $row['stuksprijs'];
    }
}

function remove_product($pid) {
    $pid = intval($pid);
    $max = count($_SESSION['cart']);
    for ($i = 0; $i < $max; $i++) {
        if ($pid == $_SESSION['cart'][$i]['productid']) {
            unset($_SESSION['cart'][$i]);
            break;
        }
    }
    $_SESSION['cart'] = array_values($_SESSION['cart']);
}

function get_order_total() {
    $max = count($_SESSION['cart']);
    $sum = 0;
    for ($i = 0; $i < $max; $i++) {
        $pid = $_SESSION['cart'][$i]['productid'];
        $q = $_SESSION['cart'][$i]['qty'];
        $price = get_price($pid);
        $sum+=$price * $q;
    }
    return $sum;
}

function addtocart($pid, $q) {
    if ($pid < 1 or $q < 1)
        return;

    if (is_array($_SESSION['cart'])) {
        if (product_exists($pid))
            return;
        $max = count($_SESSION['cart']);
        $_SESSION['cart'][$max]['productid'] = $pid;
        $_SESSION['cart'][$max]['qty'] = $q;
    }
    else {
        $_SESSION['cart'] = array();
        $_SESSION['cart'][0]['productid'] = $pid;
        $_SESSION['cart'][0]['qty'] = $q;
    }
}

function product_exists($pid) {
    $pid = intval($pid);
    $max = count($_SESSION['cart']);
    $flag = 0;
    for ($i = 0; $i < $max; $i++) {
        if ($pid == $_SESSION['cart'][$i]['productid']) {
            $flag = 1;
            break;
        }
    }
    return $flag;
}

?>