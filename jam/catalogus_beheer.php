<?php
include 'menu.php';
include 'connect-site.php';
SESSION_START();


$actie = "";
$ids = "";
$geselecteerdAccount = array();

//sessie beheerder//

if (isset($_SESSION['inlognaam'])) {
    $email1 = $_SESSION['inlognaam'];
    $preKlantID = "SELECT ingelogdStatus FROM klant WHERE email = :inlognaam";
    $klantID = $dbh->prepare($preKlantID);
    $klantID->bindValue(":inlognaam", $email1, PDO::PARAM_STR);
    $klantID->execute();
    foreach ($klantID as $klant) {
        if ($klant['ingelogdStatus'] == 3) {


            if (isset($_POST['ids'])) {
                if (isset($_POST['actie'])) {
                    $actie = $_POST['actie'];

//Verwijderen van geselecteerde klantregistratie//

                    if ($actie == "verwijderen") {

                        $ids = $_POST['ids'][0];
                        $st = "DELETE FROM product WHERE productID =ids ";

                        $sth = $dbh->prepare($st);
                        $sth->bindValue(':ids', $ids, PDO::PARAM_STR);


                        $sth->execute();
                    }
                }
            }
//Opslaan gewijzigde gegevens  waneer op opslaan wordt geklikt//			
            if ($actie == "opslaan") {
                $productID = $_POST['productID'];
                $naam = $_POST['naam'];
                $soort = $_POST['soort'];
                $stuksprijs = $_POST['stuksprijs'];
                $omschrijving = $_POST['omschrijving'];
                $plaatje = $_POST['plaatje'];
                $std = "UPDATE product SET naam='$naam',soort='$soort',stuksprijs='$stuksprijs',omschrijving='$omschrijving',plaatje='$plaatje'
                        WHERE productID=$productID";
                $stdd = $dbh->prepare($std);

                $stdd->execute();
            }
        }
//Weergeven alle klantregistraties//		
        $sth = $dbh->prepare("SELECT * FROM product");
        $sth->execute();
        $accountlijst = $sth->fetchAll(PDO::FETCH_BOTH);
        ?>
        <article multilinks-noscroll="true" id="content" class="clearfix">
            <h1>Jaminventaris</h1>

            <form action="catalogus_beheer.php" method="POST">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <td>Keuze</td>
                            <td>ProductID</td>
                            <td>Naam</td>
                            <td>Soort</td>
                            <td>Stuksprijs</td>
                            <td>Omschrijving</td>
                            <td>Plaatje</td>
                        </tr>
                    </thead>

                    <?php
//Stop alle gegevens in een tabel//
                    foreach ($accountlijst as $accountdetails) {
                        print ("
	
            <tr> 
		 <td> <input style= inline type=\"radio\" name=\"ids[]\" value=\"$accountdetails[productID]\"></td>
                 <td>" . ($accountdetails['productID']) . "</td>
		 <td>" . ($accountdetails['naam']) . "</td>
		 <td>" . ($accountdetails['soort']) . "</td>
		 <td>" . ($accountdetails['stuksprijs']) . "</td>
    	         <td>" . ($accountdetails['omschrijving']) . "</td>
		 <td>" . ($accountdetails['plaatje']) . "</td>
			</tr>	
		 ");
                    }
                    ?>	</table>
                <table>
                    <?php
//Formulier met de bestaande gegevens en een opslaan knop tonen.//			 					
                    if ($actie == "wijzigen") {
                        $nr = $_POST['ids'][0];

                        $std = $dbh->prepare("SELECT * FROM product where productID=$nr");
                        $std->execute();
                        $wijziglijst = $std->fetchAll(PDO::FETCH_BOTH);
                        foreach ($wijziglijst as $geselecteerdAccount) {

                            print("<input type=\"hidden\" name=\"productID\" value=\"$nr\"/>");
                            print("Naam:</br><input type=\"text\" value=\"" . $geselecteerdAccount['naam'] . "\" name=\"naam\"/></br>");
                            print("Soort:</br><input type=\"text\" value=\"" . $geselecteerdAccount['soort'] . "\" name=\"soort\"/></br>");
                            print("stuksprijs:</br><input type=\"text\" value=\"" . $geselecteerdAccount['stuksprijs'] . "\" name=\"stuksprijs\"/></br>");
                            print("omschrijving:</br><input type=\"text\" value=\"" . $geselecteerdAccount['omschrijving'] . "\" name=\"omschrijving\"/></br>");
                            print("plaatje:</br><input type=\"text\" value=\"" . $geselecteerdAccount['plaatje'] . "\"  name=\"plaatje\"/></br>");
                            print("<input type='submit' value='opslaan' name='actie'><br>");
                            print 'Herkies uw selectie om de wijziging te bevestigen';
                        }
                    } elseif ($actie == "opslaan") {
                        
                    }
                    ?>
                </table>
                </br>
                <div  style="text-align:center;">
                    <input class="wijzigen" type="submit" value="wijzigen" name="actie"/>
                    <input class="verwijderen" type="submit" value="verwijderen" name="actie"/>
                </div>
            </form>
        <br>
        <form action="./product_toevoegen.php" method="POST">
            <div  style="text-align:center;">
                <input class="toevoegen" type="submit" value="toevoegen" name="actieT"/>
            </div>
        </form>
        <?php
    }
} else {
    print "Login met uw beheerders account om toegang tot deze pagina te krijgen";
}
?>

</article>
</body>
</html>